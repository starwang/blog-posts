**注意：安装锐速需降级系统内核，而安装 Google BBR算法需要升级系统内核，二者不可同时安装（本文以centos6 64位为例）**



---

### 安装锐速（ServerSpeeder）破解版

#### 1. 查看系统内核版本

输入 `$ uname -r`查看当前系统内核版本（如果安装时选的centos6 64位系统的话，会出现以下文字）

<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fxh2cwq9spj30gy02uwes.jpg" align="center" alt="image">


---

#### 2. 获取并执行安装脚本

执行以下命令：

`wget --no-check-certificate -O appex.sh https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh && bash appex.sh install '2.6.32-642.el6.x86_64'`

<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fxh2eex4oij31b40e040u.jpg" align="center" alt="image">

<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fxh2faezq4j31fr0x4aiv.jpg" align="center" alt="image">
一路回车即可

出现以下内容即为安装成功

<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fxh2grabyyj314a0uugqm.jpg" align="center" alt="image">


--- 

另附centos7安装锐速破解版命令：

> 1. 更换内核 `wget --no-check-certificate -O rskernel.sh https://raw.githubusercontent.com/uxh/shadowsocks_bash/master/rskernel.sh && bash rskernel.sh`

> 2. 重启后执行安装 `yum install net-tools -y && wget --no-check-certificate -O appex.sh https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh && bash appex.sh install`



### 安装Google BBR魔改版

**centos6/7使用**

输入以下命令： `wget --no-check-certificate https://raw.githubusercontent.com/nanqinlang-tcp/tcp_nanqinlang/master/General/CentOS/bash/tcp_nanqinlang-1.3.2.sh && bash tcp_nanqinlang-1.3.2.sh`

<img src="https://wx1.sinaimg.cn/large/007jb4T5ly1fxh2i6ofl9j30yk0imn0s.jpg" align="center" alt="image">
如内核过低，需要先升级内核，再安装算法。  

安装成功后会出现以下界面：

<img src="https://wx2.sinaimg.cn/large/007jb4T5ly1fxh2jm0i9aj30m402y74t.jpg" align="center" alt="image">


---

**参考地址：[地址1](https://www.vultrcn.com/7.html) [地址2](https://www.vultrcn.com/5.html)**