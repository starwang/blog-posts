> SpringBoot使用中遇到的问题总结，持续更新...

### IDEA中读取配置文件问题
问题如图：
<img src="https://wx4.sinaimg.cn/large/007jb4T5ly1fxrefyccolj30vq06smza.jpg" align="center" alt="image">
解决方案：
引入如下依赖
```
<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
    </dependency>
```
此时会出现如下图：
<img src="https://wx3.sinaimg.cn/large/007jb4T5ly1fxrem3cjo3j31dk06idin.jpg" align="center" alt="image">
可以直接忽略。

---
### tomcat容器启动过慢的问题
启动项目时，明明一个很简单的项目，却启动很慢，tomcat日志中有这么一行：
```
WARNING: Creation of SecureRandom instance for session ID generation using [SHA1PRNG] took [20,617] milliseconds.
```
启动成功耗时：
```
INFO: Server startup in 33239 ms
```
这个警告的操作耗了大半的启动时间，查阅tomcat的[wiki](https://wiki.apache.org/tomcat/HowTo/FasterStartUp)发现这么一段话：
<img src="https://wx1.sinaimg.cn/large/007jb4T5ly1fxtkl2jdatj31xs0iqwmv.jpg" align="center" alt="image">
解释得很清楚，tomcat启动时熵源的生成默认是JRE的阻塞式熵源（/ dev / random），替换为非阻塞式熵源（/ dev /./ urandom）即可，但是会降低安全性，因为获得的随机数据较少。
修改方式：
直接修改$JAVA_HOME/jre/lib/security/java.security中
```
securerandom.source=file:/dev/./urandom
```
或者直接执行
```
java -jar app.jar -Djava.security.egd=file:/dev/./urandom
```
修改完再启动：
```
INFO: Server startup in 10847 ms
```
效果还是很显著的😊。

---
### 静态变量的注入
日常开发中，一般常用的工具会抽取到工具类中，工具类中的方法一般都是静态调用的，方法中可能会注入其他成员变量，由于方法是static的，所以成员变量也必须是静态的。此时，使用@Autowired已经无法注入，此时就需要新的操作了😏。
首先，将需要使用的定义为静态变量：
```
private static RestTemplate restTemplate;
```
然后生成set方法并在方法上加@Autowired（读取属性文件的话使用@Value）注解（注意不要加static）：
```
@Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        AuthTokenUtil.restTemplate = restTemplate;
    }
```
最后，类上加上@Component注解即可。
