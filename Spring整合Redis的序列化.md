### 序列化策略

> - #### String序列化

> StringRedisTemplate默认采用的是String的序列化策略，保存的key和value都是采用此策略序列化保存的。即StringRedisSerializer

> - #### JDK序列化

> RedisTemplate默认采用的是JDK的序列化策略，保存的key和value都是采用此策略序列化保存的。即JdkSerializationRedisSerializer

> - ### JacksonJsonRedisSerializer

> - ### Jackson2JsonRedisSerializer

> - ### OxmSerializer



---



### 可能遇到的问题

> 因为序列化策略的不同，即使是同一个key用不同的Template去序列化，结果是不同的。所以根据key去进行操作Redis中的数据时，就会出现各种操作失败的问题。



---



### 解决方案

可以手动指定RedisTemplate的key的序列化策略

```xml

<!-- redis 序列化策略 ，通常情况下key值采用String序列化策略， -->  

<!-- 如果不指定序列化策略，StringRedisTemplate的key和value都将采用String序列化策略； -->  

<!-- 但是RedisTemplate的key和value都将采用JDK序列化 这样就会出现采用不同template保存的数据不能用同一个template操作的问题 -->  

<bean id="stringRedisSerializer"   

    class="org.springframework.data.redis.serializer.StringRedisSerializer" />  

  

<bean id='redisWriteTemplate' class="org.springframework.data.redis.core.RedisTemplate">  

    <property name="connectionFactory" ref="jedisWriteConnectionFactory" />  

    <property name="keySerializer" ref="stringRedisSerializer" />  

    <property name="hashKeySerializer" ref="stringRedisSerializer" />  

</bean>  

```



### 总结

推荐将所有Template的key都采用String的序列化方式，而value的序列化方式可以采用不同的序列化方式。（jedis自动选择）(这样还有一个好处就是不必string的也采用jdk的序列化从而导常用数据格式致为了存储数据结构浪费空间)



[参考地址](https://blog.csdn.net/y666666y/article/details/70212767)