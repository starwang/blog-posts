> 记录 Typecho 或其主题有关改动。

### 评论显示 User Agent

使用插件实现，插件[地址](https://github.com/ennnnny/typecho)。

1. 下载后将其上传到网站根目录下的 `usr/plugins` 文件夹并解压，内含两个插件。

2. 这时可以登录网站管理后台配置并启用：<img src="https://ws4.sinaimg.cn/large/007jb4T5gy1g0lytvuu4aj30ue0nkwfp.jpg" alt="image" align="center">

3. 修改评论页面代码，配置 UA 显示位置，`Handsome` 主题修改主题根目录下的 `component/comments.php`，大致位置如下：

   <img src="https://wx2.sinaimg.cn/large/007jb4T5ly1g0lyz7p3itj318g0ckq5u.jpg" alt="image" align="center">

4. 查看评论效果：

   <img src="https://ws2.sinaimg.cn/large/007jb4T5ly1g0lz14o2ksj30qw06q3yt.jpg" alt="image" align="center">



### 鼠标点击特效

将一下代码插入到 `handsome/component/footer.php` 中的 `</body>` 之前：

```javascript
//字体自行修改
<script type="text/javascript"> 
/* 鼠标特效 */
var a_idx = 0; 
jQuery(document).ready(function($) { 
    $("body").click(function(e) { 
        var a = new Array("富强", "民主", "文明", "和谐", "自由", "平等", "公正" ,"法治", "爱国", "敬业", "诚信", "友善"); 
        var $i = $("<span/>").text(a[a_idx]); 
        a_idx = (a_idx + 1) % a.length; 
        var x = e.pageX, 
        y = e.pageY; 
        $i.css({ 
            "z-index": 999999999999999999999999999999999999999999999999999999999999999999999, 
            "top": y - 20, 
            "left": x, 
            "position": "absolute", 
            "font-weight": "bold", 
            "color": "#ff6651" 
        }); 
        $("body").append($i); 
        $i.animate({ 
            "top": y - 180, 
            "opacity": 0 
        }, 
        1500, 
        function() { 
            $i.remove(); 
        }); 
    }); 
}); 
</script>
```



### 头像转动并放大

鼠标焦点移动大头像上会转动并放大，将一下代码添加到主题设置中的 `自定义 CSS` 中：

```css
/*转动快慢和头像大小自行修改数值*/
/*首页头像自动旋转*/
.thumb-lg{
    width:130px;
}

.avatar{
    -webkit-transition: 0.4s;
    -webkit-transition: -webkit-transform 0.4s ease-out;
    transition: transform 0.4s ease-out;
    -moz-transition: -moz-transform 0.4s ease-out; 
}

.avatar:hover{
    transform: rotateZ(360deg);
    -webkit-transform: rotateZ(360deg);
    -moz-transform: rotateZ(360deg);
}

#aside-user span.avatar{
    animation-timing-function:cubic-bezier(0,0,.07,1)!important;
    border:0 solid
}

#aside-user span.avatar:hover{
    transform:rotate(360deg) scale(1.2);
    border-width:5px;
    animation:avatar .5s
}
```



### 文章标题居中

由于文章标题大小是 `h2`，所以替换完文章中的 `h2` 标签也会居中，将以下代码添加到主题设置的 `自定义 CSS` 中：

```css
/*文章标题居中*/
.panel h2{
    text-align: center; 
}
.post-item-foot-icon{
    text-align: center;
}
```



### 标签页失去焦点

将一下代码插入到主题文件中的随意加载文件中：

```javascript
<!-- 加入标签页失去焦点 -->
<script type="text/javascript"> 
        var title=document.title; 
// window 失去焦点 
window.onblur = function() { 
    document.title='Error 404';
};
// window 获得焦点 
window.onfocus = function() { 
    document.title='404?不存在的,这辈子不可能404'; 
    setTimeout("document.title=title",1500); 
}
</script>
```



