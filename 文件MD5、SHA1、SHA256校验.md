> 网上下载很多文件，都会提供md5或者SHA256校验，防止文件内容被篡改，文件下载到本地后，

我们可以对文件进行校验。



---



#### Windows环境下

在Windows操作系统环境下，计算这几个常用的校验值需要下载一款软件

[MD5 & SHA Checksum Utility

](https://raylin.wordpress.com/downloads/md5-sha-1-checksum-utility/)

,下载后即可使用。



---



#### Linux环境下

Linux 下有md5sum命令，sha1sum命令和sha256sum命令来计算文件相对应的值，具体使用如下：

```

[root@iz2ze0ibk1pvak9ckuwb5yz /]# md5sum test.txt 

2f6c38be914b756fde482fff83064d37 test.txt



[root@iz2ze0ibk1pvak9ckuwb5yz /]# sha1sum test.txt 

228dfdb10e9ad6fdf5ca97f402355df1952112fe test.txt





[root@iz2ze0ibk1pvak9ckuwb5yz /]# sha256sum test.txt 

d8a0785f3ce124ee8c79c172eefdc6989141aafaf9deab2dac8437ed5e60f5c4 test.txt

```



---



#### macOS环境下

mac 下有md5命令和shasum命令，具体使用如下：

```

wangyxdeMBP:~ wangyx$ md5 test.txt

MD5 (test.txt) = af7bbf6c8b822e9e89ccc2b8552ca294



wangyxdeMBP:~ wangyx$ shasum -a 1 test.txt

1367a1ab1da100436c5ea6b0dc0d737ada6aede4 test.txt



wangyxdeMBP:~ wangyx$ shasum -a 256 test.txt

5913f04a2a92cbd0346a4a7cf7856f8174e12657ca267764b57da90b27c4c51f test.txt

```



**我们可以使用计算出的校验值和网站提供的校验值对比，可以看出文件是否被篡改。**