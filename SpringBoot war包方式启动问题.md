> SpringBoot支持jar包和war包两种打包方式。jar包方式运行简单，对于轻量的项目，从构造到部署极大的提升了效率；war包方式对于启动参数、集群配置来说可能更方便一些。在使用war包方式的时候遇到了一个问题，记录下来📝。

## 前提概要
- SpringBoot版本<code>2.0.3.RELEASE</code>
- 打包方式 ```<packaging>war</packaging>```
- JDK1.8 编码UTF-8
- 依赖只引入了``` spring-boot-starter-web、spring-boot-starter-test```
- tomcat版本7.0.82

## 问题
配置好项目及Tomcat部署后启动失败，有这么一行异常信息：
```
Caused by: java.lang.NoClassDefFoundError: javax/el/ELManager
```

## 解决路径
### 缺少jar包
第一反应，缺少jar包，在pom文件引入：
```
<dependency>
    <groupId>javax.el</groupId>
    <artifactId>javax.el-api</artifactId>
    <version>3.0.0</version>
</dependency>
```
重新启动后问题仍然存在，加入` <scope>provided</scope>` 后仍然不行，<code>失败</code>。

### hibernate-validator版本过高
搜索后有文章说是 <code>当前版本的springboot，依赖的验证hibernate-validator版本太高，导致找不到ELManager。</code>查看项目依赖树，发现是web依赖中的
<img src="https://wx3.sinaimg.cn/large/007jb4T5ly1fxq7hr323fj31ag0n4wj5.jpg" align="center" alt="image">
查看其他项目中同样写法并不报错，排除这种原因。

### Tomcat版本
有人说Tomcat7提供的el-api.jar版本是2.2，Tomcat8的版本是3.0，未尝试切换Tomcat版本。

### 最终解决
将下载的el-api3.0的jar包拷贝到$TOMCAT_HOME/lib下替换之前的el-api.jar即可。

<code>参考地址：</code>[stackoverflow](https://stackoverflow.com/questions/45841464/java-lang-noclassdeffounderror-javax-el-elmanager)
