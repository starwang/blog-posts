> openfire安装完成后，后台会默认在指定的数据库中生成of* 等许多表，但是实际开发中往往需要集成已有的数据库表，可以通过更改系统属性来实现（用户认证，不包含群组）。

## 准备工作
首先，业务系统中需存在用户认证相关信息表以便集成。
```sql
create table im_user
(
	member_id bigint null,
	mobile varchar(255) null,
	email varchar(255) null,
	username varchar(255) null,
	password varchar(255) null,
	create_time timestamp null
)
```
## 更改ofproperty表
系统默认验证类：
<img src="https://wx1.sinaimg.cn/large/007jb4T5ly1fwh86yvbbyj31ie07i40l.jpg" alt="image" style="display:block;margin:0 auto;">
openfire提供了MySQL数据库的验证类，路径为org.jivesoftware.openfire.auth.JDBCAuthProvider，以及用户相关的操作类org.jivesoftware.openfire.user.JDBCUserProvider，我们可以通过新建相关属性、更改验证方式和用户相关操作来达到使用已有MySQL数据库表的目的。

执行以下SQL语句新建系统属性（同样可以在管理页面操作）：
```sql
insert  into `myopenfire`.ofproperty(name,propValue)values
('admin.authorizedJIDs','admin@localhost'),
('jdbcProvider.driver','com.mysql.jdbc.Driver'),
('jdbcProvider.connectionString','jdbc:mysql://192.168.53.61:3306/myopenfire?user=test&password=test&rewriteBatchedStatements=true&useUnicode=true&characterEncoding=utf8'),
('jdbcAuthProvider.passwordSQL','SELECT password FROM im_user WHERE mobile=?'),
('jdbcAuthProvider.passwordType','plain'),
('jdbcUserProvider.loadUserSQL','SELECT username,email FROM im_user WHERE mobile=?'),
('jdbcUserProvider.userCountSQL','SELECT COUNT(*) FROM im_user'),
('jdbcUserProvider.allUsersSQL','SELECT username FROM im_user'),
('jdbcUserProvider.usernameField','username'),
('jdbcUserProvider.nameField','mobile'),
('jdbcUserProvider.emailField','email');
```
将系统属性更改为以上两个类即可。
> admin.authorizedJIDs属性即管理员账户名称，@后是初始配置的域，必须带上，并且im_user表中必须有admin相关的验证信息才能登录管理页面。

参考：[官方文档](http://download.igniterealtime.org/openfire/docs/latest/documentation/db-integration-guide.html)