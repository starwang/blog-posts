> 在 MySQL数据库中，我们常用的utf-8格式编码最多支持 3 个字节，而 emoji表情是 4 个字节的符号，所以 emoji表情的长度就超出了我们常用utf-8的字符集范围而无法存储。从MySQL5.5开始，可以更改数据库和数据表的编码为utf8mb4使其支持存储emoji，utf8mb4兼容utf8，且比utf8能表示更多的字符，是utf8字符集的超集。

> 修改之前一定要备份数据！修改之前一定要备份数据！修改之前一定要备份数据！

#### 1. 更改数据库的排序规则
![order-rule](https://wx3.sinaimg.cn/large/007jb4T5ly1fvfzrk6j8ij31yq10fain.jpg)

#### 2. 点击SQL按钮，执行以下SQL语句(将typecho修改为自己对应的前缀)
```sql
alter table typecho_comments convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table typecho_contents convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table typecho_fields convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table typecho_metas convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table typecho_options convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table typecho_relationships convert to character set utf8mb4 collate utf8mb4_unicode_ci;
alter table typecho_users convert to character set utf8mb4 collate utf8mb4_unicode_ci;
```
修改完之后：
![after-modified](https://ws4.sinaimg.cn/large/007jb4T5ly1fvg105ehbxj31ec0mo11c.jpg)

#### 3. 编辑网站根目录的config.inc.php文件，文件末尾
```php
/** 定义数据库参数 */
$db = new Typecho_Db('Pdo_Mysql', 'typecho_');
$db->addServer(array (
  'host' => 'localhost',
  'user' => 'username',
  'password' => 'password',
  'charset' => 'utf8mb4',// 将utf-8修改为utf8mb4
  'port' => '3306',
  'database' => 'database-name',
), Typecho_Db::READ | Typecho_Db::WRITE);
Typecho_Db::set($db);
```
按照以上步骤修改完之后，文章即可支持emoji表情。